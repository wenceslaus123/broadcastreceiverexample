package com.sourceit.broadcastreceiverexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by wenceslaus on 26.06.18.
 */

public class GlobalBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "GlobalBroadcastReceiver!", Toast.LENGTH_SHORT).show();
    }
}
