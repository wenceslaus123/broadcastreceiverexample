package com.sourceit.broadcastreceiverexample;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public class MyService extends Service {

    public static final String EVENT_KEEP_GOING = "com.sourceit.keep_going";

    Handler h = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Intent intent = new Intent(EVENT_KEEP_GOING);
            sendOrderedBroadcast(intent, null);
            h.sendEmptyMessageDelayed(1, 5_000);
            return false;
        }
    });

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        h.sendEmptyMessageDelayed(1, 3_000);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        h.removeMessages(1);
    }
}
